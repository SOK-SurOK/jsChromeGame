import Phaser from 'phaser'
import {whatFileUrlUse} from "../../secondaryFunction"
import floorImg from "../assets/floor.png"
import pcImg from "../assets/pc.png"
import spamImg from "../assets/spam.jpg"
import fishImg from "../assets/fish.jpg"
import passwordImg from "../assets/password.jpg"
import socialImg from "../assets/social.jpg"
import systemImg from "../assets/system.jpg"
import ScoreLabel from "./ui"
import bossNormImg from "../assets/bossNorm.png"
import bossBadImg from "../assets/bossBad.png"
import bossGoodImg from "../assets/bossGood.png"
import bossBad2Img from "../assets/bossBad2.png"
import bossGood2Img from "../assets/bossGood2.png"
import bossDeadImg from "../assets/bossDead.png"

const PC_KEY = 'pc'
const KEYS = ['spam', 'fish', 'password', 'social', 'system']
const MESSAGES = [
    ['разрешить прислать \nв офис кучу \nдешевой говядинки', 'показать кучу \nпредложений о\nпокупке дешевой \nговядинки'],
    ['использовать \nзнакомый неудобный \nсайт для покупки рыбы', 'перейти на новый \nадрес рыбного сайта'],
    ['использовать \nкорявый пароль', 'использовать легко \nзапоминающий \nпароль'],
    ['проверять все \nи не верить \nникому', 'скачать программу \nчерез быстрое \nнеофициальное облако'], //соц инженерия
    ['перейти на linux', 'периодически \nпереустанавливать \nвсю систему']
]

export default class GameScene extends Phaser.Scene {

    constructor() {
        super('game-scene')

        this.boss = undefined
        this.scoreLabel = undefined
        this.gameOver = false

        this.badGroup = undefined
        this.goodGroup = undefined

        this.statusSpawn = undefined
        this.timer = undefined

        this.positionsPC = [[105, 105], [105, 545], [695, 105], [695, 545]]
        this.textPC = undefined
        // this.imagePC = undefined
        this.textConfig = {fontSize: '17px', color: '#000000', fontFamily: 'Arial'}
        this.textConfig2 = {fontSize: '30px', color: '#ff0000', fontFamily: 'Arial'}
        this.timerBaseDelay = 100  //0.1s
        this.timerCount = 0
        this.timerLoop = 50
        this.LoopCount = 0
        this.LoopStatic = 10
    }

    preload() {
        const srcUrl = whatFileUrlUse()
        if (srcUrl == null) {
            this.load.image('floor', floorImg)
            this.load.image(PC_KEY, pcImg)
            this.load.image(KEYS[0], spamImg)
            this.load.image(KEYS[1], fishImg)
            this.load.image(KEYS[2], passwordImg)
            this.load.image(KEYS[3], socialImg)
            this.load.image(KEYS[4], systemImg)

            this.load.spritesheet('bossNorm',
                bossNormImg,
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossGood',
                bossGoodImg,
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossBad',
                bossBadImg,
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossGood2',
                bossGood2Img,
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossBad2',
                bossBad2Img,
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossDead',
                bossDeadImg,
                {frameWidth: 104, frameHeight: 110}
            )
        } else {
            this.load.setBaseURL(srcUrl)
            this.load.image('floor', 'floor.png')
            this.load.image(PC_KEY, 'pc.png')
            this.load.image(KEYS[0], 'spam.jpg')
            this.load.image(KEYS[1], 'fish.jpg')
            this.load.image(KEYS[2], 'password.jpg')
            this.load.image(KEYS[3], 'social.jpg')
            this.load.image(KEYS[4], 'system.jpg')
            this.load.spritesheet('bossNorm',
                'bossNorm.png',
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossGood',
                'bossGood.png',
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossBad',
                'bossBad.png',
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossGood2',
               'bossGood2.png',
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossBad2',
                'bossBad2.png',
                {frameWidth: 104, frameHeight: 110}
            )
            this.load.spritesheet('bossDead',
                'bossDead.png',
                {frameWidth: 104, frameHeight: 110}
            )
        }
    }

    create() {
        // this.input.setPollAlways(); // всегда проверять мышь на коллизии

        this.cameras.main.setBackgroundColor("#ffffff")
        let floor = this.physics.add.staticImage(400, 25, 'floor')

        this.boss = this.createBoss()
        this.boss.anims.play('boss_norm')

        const pc = this.createPC(this.positionsPC)
        this.statusSpawn = this.createStatusSpawn(this.positionsPC)
        // this.imagePC = this.createImagePC(this.positionsPC)
        this.textPC = this.createTextPC(this.positionsPC)

        this.badGroup = this.physics.add.group()
        this.goodGroup = this.physics.add.group()

        this.scoreLabel = this.createScoreLabel(150, 7, 0)

        this.physics.add.collider(this.badGroup, pc)
        this.physics.add.collider(this.goodGroup, pc)
        this.physics.add.collider(this.badGroup, floor)
        this.physics.add.collider(this.goodGroup, floor)
        this.physics.add.overlap(this.boss, this.badGroup, this.hitBad, null, this)
        this.physics.add.overlap(this.boss, this.goodGroup, this.hitGood, null, this)

        this.boss.on(Phaser.Animations.Events.SPRITE_ANIMATION_COMPLETE, function (anim) {
            //console.log("закончилась анимация: ", anim.key)
            if (!this.gameOver) {
                switch (anim.key) {
                    case 'boss_norm':
                        break
                    case 'boss_bad':
                        this.scoreLabel.sub(15)
                        this.boss.clearTint()
                    case 'boss_bad2':
                        this.scoreLabel.sub(5)
                        if (this.scoreLabel.getScore() < 0) {
                            this.gameOver = true
                            this.timer.paused = true
                            this.physics.pause()
                            this.boss.anims.play('boss_dead', true)
                            this.add.text(300, 200, "GAME OVER", this.textConfig2)
                        } else {
                            this.boss.anims.play('boss_norm')
                        }
                        break
                    case 'boss_good':
                        this.scoreLabel.add(5)
                    case 'boss_good2':
                        this.scoreLabel.add(1)
                        this.boss.anims.play('boss_norm')
                        break
                    case 'boss_dead':
                        break
                    default:
                        break
                }
            }
        }, this)

        this.boss.on(Phaser.Animations.Events.SPRITE_ANIMATION_REPEAT, function (anim) {
            //console.log("повторяется анимация: ", anim.key)
            if (!this.gameOver) {
                switch (anim.key) {
                    case 'boss_norm':
                        break
                    case 'boss_bad':
                        this.scoreLabel.sub(10)
                    case 'boss_bad2':
                        this.scoreLabel.sub(10)
                        if (this.scoreLabel.getScore() < 0) {
                            this.gameOver = true
                            this.timer.paused = true
                            this.physics.pause()
                            this.boss.anims.play('boss_dead', true)
                            this.add.text(300, 200, "GAME OVER", this.textConfig2)
                        }
                        break
                    case 'boss_good':
                        this.scoreLabel.add(5)
                    case 'boss_good2':
                        this.scoreLabel.add(1)
                        break
                    case 'boss_dead':
                        break
                    default:
                        break
                }
            }
        }, this)

        // работает только для интерактивных элементов
        this.input.on('gameobjectdown', function (pointer, gameObject) {
            if (!this.gameOver) {
                // gameObject.setTint(Math.random() * 16000000)
                if (gameObject.myStatus.isGood === true) {
                    this.boss.anims.play('boss_bad2')
                } else {
                    this.boss.anims.play('boss_good2')
                }
                // this.imagePC[gameObject.myStatus.idPC].setVisible(false)
                this.statusSpawn[gameObject.myStatus.idPC] = 'no'
                this.textPC[gameObject.myStatus.idPC].setText('...')

                gameObject.destroy()
            }
        }, this)

        this.timer = this.time.addEvent({
            delay: this.timerBaseDelay, // ms
            callback: this.timerFunction,
            args: [this.statusSpawn],
            callbackScope: this,
            loop: true
        })
    }

    createBoss() {
        const boss = this.physics.add.staticSprite(400, 300, 'bossNorm')

        this.anims.create({
            key: 'boss_norm',
            frames: 'bossNorm',
            frameRate: 10,
            repeat: -1
        })
        this.anims.create({
            key: 'boss_bad',
            frames: 'bossBad',
            frameRate: 15,
            repeat: 0
        })
        this.anims.create({
            key: 'boss_good',
            frames: 'bossGood',
            frameRate: 10,
            repeat: 0
        })
        this.anims.create({
            key: 'boss_bad2',
            frames: 'bossBad2',
            frameRate: 10,
            repeat: 1
        })
        this.anims.create({
            key: 'boss_good2',
            frames: 'bossGood2',
            frameRate: 10,
            repeat: 0
        })
        this.anims.create({
            key: 'boss_dead',
            frames: 'bossDead',
            frameRate: 10,
            repeat: -1
        })

        return boss; //возвращаем экземпляр
    }

    createPC(positionsPC) {
        const pc = this.physics.add.staticGroup();
        positionsPC.forEach(function (item, i, arr) {
            pc.create(item[0], item[1], PC_KEY)
        })
        return pc; //возвращаем экземпляр
    }

    createStatusSpawn(positionsPC) {
        return 'no '.repeat(positionsPC.length).trim().split(" ")
    }

    createTextPC(positionsPC) {
        let arr = []
        for (let i = 0; i < positionsPC.length; i++) {
            arr[i] = this.add.text(positionsPC[i][0] - 92, positionsPC[i][1] - 45, "...", this.textConfig)
        }
        return arr
    }

    createScoreLabel(x, y, score) {
        const style = {fontSize: '32px', fill: '#000'}
        const label = new ScoreLabel(this, x, y, score, style);

        this.add.existing(label);

        return label;
    }

    timerFunction(statusSpawn) {
        //console.log(this.timerCount, this.timerLoop, this.LoopCount)
        this.timerCount += 1
        if (this.timerCount === this.timerLoop) {
            this.timerCount = 0
            this.LoopCount += 1
            if (this.LoopCount === this.LoopStatic) {
                this.LoopCount = 0
                if (this.timerLoop > 1) {
                    this.timerLoop -= 1 // 1
                }
            }
            // spawn
            for (let i = 0; i < statusSpawn.length; i++) {
                let rnd = Phaser.Math.Between(0, statusSpawn.length - 1)
                if (statusSpawn[rnd] === 'no') {
                    let rnd2 = -1
                    for (let j = 0; j < KEYS.length; j++) {
                        let rnd22 = Phaser.Math.Between(0, KEYS.length - 1)
                        if (statusSpawn.indexOf(KEYS[rnd22]) === -1) {
                            rnd2 = rnd22
                            break
                        }
                    }
                    if (rnd2 !== -1) {
                        statusSpawn[rnd] = KEYS[rnd2]
                        const choice = Math.random() > 0.5
                        //console.log(choice)
                        this.spawn(choice ? this.goodGroup : this.badGroup, statusSpawn[rnd], rnd, choice, rnd2)
                    }
                    break
                }
            }
        }
    }

    /**
     * @param {Phaser.Physics.Arcade.Group} group
     * @param {string} key
     * @param {number} iPC
     * @param {boolean} choice
     * @param {number} keyId
     */
    spawn(group, key, iPC, choice, keyId) {
        if (choice) {
            this.textPC[iPC].setText(MESSAGES[keyId][0])
        } else {
            this.textPC[iPC].setText(MESSAGES[keyId][1])
        }
        const message = group.create(this.positionsPC[iPC][0], this.positionsPC[iPC][1], key)
        message.setBounce(1)
        message.setCollideWorldBounds(true)
        message.setVelocity(Phaser.Math.Between(-100, 100), 100)
        message.setInteractive()
        message.myStatus = {
            isGood: choice,
            idPC: iPC
        }
        return message
    }

    hitBad(boss, bad) {
        this.statusSpawn[bad.myStatus.idPC] = 'no'
        // this.imagePC[bad.myStatus.idPC].setVisible(false)
        this.textPC[bad.myStatus.idPC].setText('...')
        bad.destroy()

        boss.setTint(0xff0000)
        boss.anims.play('boss_bad', true)
    }

    hitGood(boss, good) {
        this.statusSpawn[good.myStatus.idPC] = 'no'
        this.textPC[good.myStatus.idPC].setText('...')
        good.destroy()

        boss.anims.play('boss_good', true)
    }
}