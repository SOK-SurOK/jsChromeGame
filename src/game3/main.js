import Phaser from 'phaser'
import GameScene from './scenes/GameScene'

const config = {
    type: Phaser.AUTO,
    parent: 'content',
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            // gravity: {y: 1},
            // debug: true
        }
    },
    scene: [GameScene],
    title: '3',
    version: '1.0b'
}

export default config