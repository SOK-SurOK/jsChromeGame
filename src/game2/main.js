import Phaser from 'phaser'
import HelloWorldScene from './scenes/HelloWordScene'

const config = {
    type: Phaser.AUTO,
    parent: 'content',
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 200}
        }
    },
    scene: [HelloWorldScene],
    title: 'Cosmos',
    version: '1.2b'
}

export default config