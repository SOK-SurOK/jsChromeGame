import Phaser from 'phaser'
import GameScene1 from './scenes/GameScene1'

const config = {
    type: Phaser.AUTO,
    parent: 'content',
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 300}
        }
    },
    scene: [GameScene1],
    title: 'DeadSec',
    version: '1.0b'
}

export default config