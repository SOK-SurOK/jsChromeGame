import Phaser from 'phaser'
import skyImg from "../assets/sky.png"
import platformImg from "../assets/platform.png"
import starImg from "../assets/star.png"
import bombImg from "../assets/bomb.png"
import dudeImg from "../assets/dude.png"
import ScoreLabel from './ui.js'
import BombSpawner from './BombSpawner'
import {whatFileUrlUse} from "../../secondaryFunction"

const GROUND_KEY = 'ground'
const DUDE_KEY = 'dude'
const STAR_KEY = 'star'
const BOMB_KEY = 'bomb';

export default class GameScene1 extends Phaser.Scene {

    constructor() {
        super('game-scene')

        this.player = undefined
        this.cursors = undefined
        this.scoreLabel = undefined
        this.stars = undefined
        this.bombSpawner = undefined
        this.gameOver = false
    }

    preload() {
        const srcUrl = whatFileUrlUse()
        if (srcUrl == null) {
            this.load.image('sky', skyImg)
            this.load.image(GROUND_KEY, platformImg)
            this.load.image(STAR_KEY, starImg)
            this.load.image(BOMB_KEY, bombImg)
            this.load.spritesheet(DUDE_KEY,
                dudeImg,
                {frameWidth: 32, frameHeight: 48}
            )
        } else {
            this.load.setBaseURL(srcUrl)
            this.load.image('sky', 'sky.png')
            this.load.image(GROUND_KEY, 'platform.png')
            this.load.image(STAR_KEY, 'star.png')
            this.load.image(BOMB_KEY, 'bomb.png')
            this.load.spritesheet(DUDE_KEY,
                'dude.png',
                {frameWidth: 32, frameHeight: 48}
            )
        }
    }

    create() {
        this.add.image(400, 300, 'sky')

        //Теперь получаем из методов экземпляры объектов для платформы и игрока
        const platforms = this.createPlatforms()
        this.player = this.createPlayer()
        // добавляем константу для хранения звезд
        this.stars = this.createStars()
        // Создаем метку по координатам (16;16) с нулевым счетом
        this.scoreLabel = this.createScoreLabel(16, 16, 0)
        this.bombSpawner = new BombSpawner(this, BOMB_KEY)
        // Создаем константу для хранения бомб
        const bombsGroup = this.bombSpawner.group

        this.physics.add.collider(this.player, platforms)
        this.physics.add.collider(this.stars, platforms);
        // Проверяем коллизии между бомбами и платформами
        this.physics.add.collider(bombsGroup, platforms)
        //бомба попадает в игрока
        this.physics.add.collider(this.player, bombsGroup, this.hitBomb, null, this);
        this.physics.add.overlap(this.player, this.stars, this.collectStar, null, this)

        // функция добавляет четыре свойства к объекту cursors
        this.cursors = this.input.keyboard.createCursorKeys()
    }

    // проверяем статус клавиш
    update() {
        //если игра окончена, то не обновляем
        if (this.gameOver) {
            return;
        }

        if (this.cursors.left.isDown) {
            this.player.setVelocityX(-160);

            this.player.anims.play('left', true);
        } else if (this.cursors.right.isDown) {
            this.player.setVelocityX(160);

            this.player.anims.play('right', true);
        } else {
            this.player.setVelocityX(0)

            this.player.anims.play('turn');
        }

        if (this.cursors.up.isDown && this.player.body.touching.down) {
            this.player.setVelocityY(-330);
        }
    }

    createPlatforms() {
        const platforms = this.physics.add.staticGroup();

        platforms.create(400, 568, GROUND_KEY).setScale(2).refreshBody();

        platforms.create(600, 400, GROUND_KEY);
        platforms.create(50, 250, GROUND_KEY);
        platforms.create(750, 220, GROUND_KEY);

        return platforms; //возвращаем экземпляр
    }

    createPlayer() {
        //формируем объект для возврата из метода
        const player = this.physics.add.sprite(100, 450, DUDE_KEY);
        player.setBounce(0.2);
        player.setCollideWorldBounds(true);

        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers(DUDE_KEY, {start: 0, end: 3}),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'turn',
            frames: [{key: DUDE_KEY, frame: 4}],
            frameRate: 20
        });

        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers(DUDE_KEY, {start: 5, end: 8}),
            frameRate: 10,
            repeat: -1
        });

        return player; //возвращаем экземпляр
    }

    createStars() {
        const stars = this.physics.add.group({
            key: STAR_KEY,
            repeat: 11,
            setXY: {x: 12, y: 0, stepX: 70}
        });

        stars.children.iterate((child) => {
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
        });

        return stars;
    }

    collectStar(player, star) {
        // отключаем физическое тело звезды
        star.disableBody(true, true);
        // добавляем 10 очков к счету
        this.scoreLabel.add(10);
        // пересоздаем звезды, когда все соберем
        if (this.stars.countActive(true) === 0) {
            //  Новый пакет звезд в коллекцию
            this.stars.children.iterate((child) => {
                child.enableBody(true, child.x, 0, true, true)
            })
        }

        this.bombSpawner.spawn(player.x);
    }

    //Метод создающий метку
    createScoreLabel(x, y, score) {
        const style = {fontSize: '32px', fill: '#000'}
        const label = new ScoreLabel(this, x, y, score, style);

        this.add.existing(label);

        return label;
    }

    hitBomb(player, bomb) {
        this.physics.pause();

        player.setTint(0xff0000);

        player.anims.play('turn');

        this.gameOver = true;
    }
}