function colorBackground() {
    let currentColor = [255, 255, 255]
    let nextColor = getRandomColor()
    let currentStep = 0
    const steps = 100

    function getRandomColor() {
        const color = []
        while (color.length < 3) {
            color.push(Math.floor(Math.random() * 255))
        }
        return color
    }

    document.body.onmousemove = function () {
        currentStep++
        document.body.style.backgroundColor = 'rgb( ' + currentColor.map(
            function (e, i) {
                return Math.floor(e + (nextColor[i] - e) * currentStep / steps)
            }).join(', ') + ')'
        if (currentStep === 100) {
            currentStep = 0
            currentColor = nextColor
            nextColor = getRandomColor()
        }
    }
}

function whatFileUrlUse() {
    if (typeof chrome == "undefined" || chrome == null) {
        return null
    } else {
        // this.load.setBaseURL('http://labs.phaser.io');
        //return eval("chrome.extension.getURL('images');")
        return chrome.extension.getURL('assets')
    }
}

export {colorBackground, whatFileUrlUse}