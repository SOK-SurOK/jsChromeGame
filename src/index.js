import {colorBackground} from "./secondaryFunction"
import Phaser from "phaser"
import config1 from "./game1/main"
import config2 from "./game2/main"
import config3 from "./game3/main"

let game = null

function updateGame(number) {
    if (game != null) {
        game.destroy(true)
    }
    switch (number) {
        case 0:
            if (game != null) {
                game = null
                // const gl = getContext()
                // gl.clear(gl.COLOR_BUFFER_BIT)
            }
            break
        case 1:
            game = new Phaser.Game(config1)
            break
        case 2:
            game = new Phaser.Game(config2)
            break
        case 3:
            game = new Phaser.Game(config3)
            break
    }
}

function setButtonOnclick (buttonId, number) {
    const myButton = document.getElementById(buttonId)
    myButton.onclick = myButton.onclick = function () {
        updateGame(number)
    }
}

setButtonOnclick("b1",1)
setButtonOnclick("b2",2)
setButtonOnclick("b3",3)
setButtonOnclick("b0",0)

colorBackground()